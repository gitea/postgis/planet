/* j/k navigation */
document.addEvent('keypress', function(ev) {
    ev = new Event(ev);
    var prop = {j:'getNext', k:'getPrevious'}[ev.key];
    if (!prop)
        return;
    var current = $$('.entrygroup.current')[0];
    if (current) {
        current.removeClass('current');
        current = current[prop]('.entrygroup')
    } else {
        var current = $$('.entrygroup')[0];
    }
    if (current) {
        current.addClass('current');
        window.location.hash = current.getProperty('id');
    }
})